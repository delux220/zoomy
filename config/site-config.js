module.exports = {
  siteUrl: 'https://hungry-shockley-09fc7a.netlify.com',
  sitemapPath: '/sitemap.xml',
  title: 'Gatsby DatoCMS Starter',
  description: 'Website Description',
  author: '@chase_ohlson',
};
